# -*- indent-tabs-mode: t -*-

ROOT != pwd
SH != which sh
SHELL != which emacs
.SHELLFLAGS = -Q --batch -l package --eval

# Files/folders

PKGDIR = packages
PUBDIR = public

ARCHIVE = $(PUBDIR)/archive-contents
INDEX = $(PUBDIR)/index.html
DIR_LOCALS = $(PUBDIR)/.dir-locals.el

PKG_DIRS = $(wildcard $(PKGDIR)/*)
PKG_NAMES = $(patsubst $(PKGDIR)/%,%,$(PKG_DIRS))

# Targets

.ONESHELL:
.PHONY: all clean $(PKG_NAMES)

all: $(ARCHIVE) $(INDEX) $(DIR_LOCALS)

$(PKG_NAMES): SHELL = $(SH)
$(PKG_NAMES): .SHELLFLAGS = -c
$(PKG_NAMES): | $(PUBDIR)
	@PATH=$$PATH:~/.local/bin
	cd $(PKGDIR)/$@
	eldev package -e -F --output-dir=$(ROOT)/$(PUBDIR)
	eldev info > $(ROOT)/$(PUBDIR)/$@-readme.txt

$(ARCHIVE): $(PKG_NAMES) | $(PUBDIR)
	@(with-temp-file "$@"
	   (pp (cons 1 (mapcar
			(lambda (entry-file)
			  (with-temp-buffer
			    (insert-file-contents entry-file)
			    (delete-file entry-file)
			    (read (current-buffer))))
			(directory-files "$(PUBDIR)" t "\.entry\\'")))
	       (current-buffer)))

$(PUBDIR): SHELL = $(SH)
$(PUBDIR): .SHELLFLAGS = -c
$(PUBDIR):
	@mkdir -p "$@"

$(INDEX): README.org | $(PUBDIR)
	@(progn
	   (setq org-confirm-babel-evaluate nil)
	   (find-file "$<")
	   (org-export-to-file (quote html) "$@"))

$(DIR_LOCALS): | $(PUBDIR)
	@(with-temp-file "$@"
	   (pp (quote ((nil (buffer-read-only . t))))
	       (current-buffer)))

clean:
	@(when (file-exists-p "$(PUBDIR)")
	   (delete-directory "$(PUBDIR)" t))
